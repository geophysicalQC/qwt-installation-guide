# README #

I created batch file for QWT installation from source. Also I provided the txt file guide of steps to make QWT work on my Windows 7/10 + Visual Studio 2015 environment.
If you find it is useful please let me know and I would be happy.

### What is this repository for? ###

Instructions on QWT installation and application.

### How do I get set up? ###

Open QWT installation guide.txt file and follow steps.

### Who do I talk to? ###

Chen Qi, geophysics.chen.qi@gmail.com, Admin